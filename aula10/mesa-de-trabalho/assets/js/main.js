
var formulario = document.querySelector('#formularioDeCadastro');

function rotina(event){

    var valorInputDigitado = document.querySelector('.c-form__campo');
    var valorInputSemEspacos = valorInputDigitado.value.replace(/\s/g, '');
    var valorInputSemNumeros = valorInputSemEspacos.replace(/\d/g, '');
    
    var listaCadastrados = document.querySelector('.c-lista__item'); 
    listaCadastrados.innerText = `Último item adicionado: ${valorInputSemNumeros}`;
    valorInputDigitado.value = '';
    event.preventDefault();
}

document.addEventListener('submit', (event) => {
    rotina(event); 
});



